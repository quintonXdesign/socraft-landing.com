<?php
require_once 'Mandrill.php'; //Not required with Composer

try{
	$mandrill = new Mandrill('XCKJARo5hEcrRSt-yqCQZQ');

	// *** Message Content

	$emailSubject = 'Walk-Ons Franchise Inquiry';
	$fromEmail = $_POST['from'];
	$emailAddresses = 'gibby@thinkx.net,franchise@walk-ons.com';
	$sendTo = explode(',', $emailAddresses);
	$personsName = $_POST['name'];
	$phoneNumber = $_POST['phone'];
	$streetAddress = $_POST['address'];
	$interest = $_POST['interest'];
	$position = $_POST['position'];
	$locations = $_POST['locations'];
	$comments = $_POST['message'];
	$htmlComments = str_replace("\n", '<br />', $comments);

	$thankYouUrl = "/thanks" ;

	$textContent = "This email was sent from $fromEmail\n" .
		"$personsName \n" .
		"$phoneNumber \n" .
		"$streetAddress \n" .
		"$interest \n" .
		"$position \n" .
		"$locations \n" .
		"$comments \n"
	;

	$htmlContent = "<p><strong>$personsName</strong><p>
			<p>$fromEmail<p>
			<p>$phoneNumber</p> 
			<p>$streetAddress</p>
			<p>$interest</p>
			<p>$position</p>
			<p>$locations</p> 
			<p>$htmlComments</p>";

	$replyTo = $fromEmail . "," . $emailAddresses;

// Testing Vars
//var_dump($replyTo);die();

	$message = array(
		'subject' => $emailSubject,
		'from_name' => $personsName,
		'from_email' => $fromEmail,
		'html' => $htmlContent,
		'text' => $textContent,
		'to' => array(
			array('email' => $sendTo[0]),
			array('email' => $sendTo[1])
		)
	);

	$async = false;
	$ip_pool = null;
	$send_at = null;

	//$result = $mandrill->messages->send($message, $async, $ip_pool, $send_at);

	$mandrill->messages->send($message, $async, $ip_pool, $send_at);

	header( "Location: $thankYouUrl" );
	//print_r($result);
}
catch(Mandrill_Error $e){

	$errorUrl = "/error" ;
	header( "Location: $errorUrl" );
	// Mandrill errors are thrown as exceptions
    // echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    // throw $e;
}
?>