<?php
require_once 'Mandrill.php'; //Not required with Composer

try{
	if (isset($_POST['mailing-button'])) {
	$mandrill = new Mandrill('sPvQvPG9BFHJ9-JpQlZBzg');

	// *** Message Content

	$emailSubject = 'New Southern Craft Beer Mailing List';
	$fromEmail = $_POST['email'];

	$emailAddresses = 'Joe.Picou@socraftbeer.com,wes.hedges@socraftbeer.com,quinton@thinkx.net';
	//$emailAddresses = 'quinton@thinkx.net';
	$sendTo = explode(',', $emailAddresses);
	//$sendTo = array('quinton@thinkx.net');
	// Testing Vars

	// var_dump($fromEmail);var_dump($sendTo);die();
	// var_dump($fromEmail); die();
	$to = array();
	foreach ($sendTo as $email) {
		$to[] = array('email' => $email);
	}



	// $pic = array();
	// foreach ($variable as $key => $value) {
	// 	# code...
	// }


	// $personsName = $_POST['name'];
	// $phoneNumber = $_POST['phone'];
	// $inquiryType = $_POST['vet-name'];
	// $location = $_POST['location'];
	// $comments = $_POST['story'];
	// $vetPic = $_POST['vet-pic'];
	// $htmlComments = str_replace("\n", '<br />', $comments);

	$thankYouUrl = "/thanks.html" ;

	// $uploaddir = '../form/uploads/';
	// $uploadfile = $uploaddir . basename($_FILES['vet-pic']['name']);
	// $finalPic = base64_encode(file_get_contents($_FILES['vet-pic']['tmp_name']));
	// $namePic = $_FILES['vet-pic']['name'];
 //    $mimePic = $_FILES['vet-pic']['type'];
 //    // 'images' => array(
 // //        array(
 // //            'type' => 'image/png',
 // //            'name' => 'IMAGECID',
 // //            'content' => 'ZXhhbXBsZSBmaWxl'
 // //        )
 // //    )

	// echo '<pre>';
	// if (move_uploaded_file($_FILES['vet-pic']['tmp_name'], $uploadfile)) {
	//     echo "File is valid, and was successfully uploaded.\n";
	// } else {
	//     echo "Possible file upload attack!\n";
	// }

	// echo 'Here is some more debugging info:';
	// print_r($_FILES);

	// print "</pre>";


	$textContent = /*"This email was sent from $fromEmail\n" .
		"$personsName \n" .
		"$phoneNumber \n" .
		"$inquiryType \n" .
		"$location \n" .
		"$comments \n" .
		"$vetPic \n"*/
		"This is the textContent variable"
	;

	$htmlContent = /*"<p>Name: <strong>$personsName</strong><p>
			<p>Email: $fromEmail<p>
			<p>Number: $phoneNumber</p> 
			<p>Veteran's Name: $inquiryType</p> 
			<p>General Location: $location</p>
			<p>Veteran's Story: $htmlComments</p>
			<p>Veteran's Image: $namePic</p>"*/
			"Something's brewing on your website. " . $fromEmail . "has just subscribed";
			

	$replyTo = $fromEmail . "," . $emailAddresses;



	$message = array(
		'headers' => array(
			'Reply-To' => $replyTo 
		),
		'subject' => $emailSubject,
		// 'from_name' => $personsName,
		'from_email' => $fromEmail,
		'html' => $htmlContent,
		'text' => $textContent,
		'bcc_address' => '',
		'to' => $to,
		// 'images' => array(
		//             array(
		//                 'type' => $mimePic,
		//                 'name' => $namePic,
		//                 'content' => $finalPic
		//             )
		//         )
	);

	$async = true;
	$ip_pool = null;
	$send_at = null;

	// var_dump($uploadfile);die();
	$result = $mandrill->messages->send($message, $async, $ip_pool, $send_at);

	//$mandrill->messages->send($message, $async, $ip_pool, $send_at);

	 // var_dump($result); die();

	header( "Location: $thankYouUrl" );
	//print_r($result);
}
}
catch(Mandrill_Error $e){

	// $errorUrl = "/error" ;
	// header( "Location: $errorUrl" );
	// Mandrill errors are thrown as exceptions
    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    throw $e;
}
?>