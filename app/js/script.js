/* Author: John Gibby @thatgibbyguy */

// ==========================
/* Store current location */
// ==========================

var pathName = location.pathname;

// ==========================
/* Remove Phone Link on Desktop */
// ==========================

remPhoneLink = function(){
	docWidth = $(document).width();
	if (docWidth >= 1024){
		$('[data-query="phone"]').click(function(){
			return false;
		});
	}
	else{
		$('[data-query="phone"]').click(function(){
			return true;
		});
	}
};
remPhoneLink();

// ==========================
/* Adding and removing classes */
// ==========================

var addClassTo = function(query){
	$('[data-query="' + query + '"]').addClass('on');
};
var removeClassFrom = function(query){
	$('[data-query="' + query + '"]').removeClass('on');
};
var addClassByUrl = function(path,query){
	if(pathName.indexOf(path) >= 0){
		$('[data-query="' + query + '"]').addClass('on');
	}
};

// ==========================
/* Mobile Menu Trigger */
// ==========================

var nav = '';
var toggler = document.getElementById('mobile-toggle');

$(toggler).click(function(){
	$(nav).toggleClass('mobile-hidden').toggleClass('show');
});

// ==============================================
/* JVFloat - Placeholder Effect*/
// ==============================================

//apply effect
if(!$('html').hasClass('lt-ie9')){
	$('.float-pattern').jvFloat();
}

// ==============================================
/* back to top scrolling */
// ==============================================


if($(window).width() > 767){
	var offset = 220;
	var duration = 500;
	$(window).scroll(function() {
		if ($(this).scrollTop() > offset) {
			$('#back-to-top').fadeIn(duration);
		} else {
			$('#back-to-top').fadeOut(duration);
		}
	});
	
	$('#back-to-top').click(function(e) {
		e.preventDefault();
		$('html, body').animate({scrollTop: 0}, duration);
		return false;
	})
}

// ==============================================
/* agecheck */
// ==============================================

$('#verify-button').click(function(){
	// $('body').removeClass('verify');
	// $('#headline-container .hide').removeClass('hide');
	// $('#headline-container .verify').addClass('hide');
	// $('a.btn.hide').removeClass('hide');
	// $('a.btn.verify').addClass('hide');
	$('#verify').fadeOut();
});

// $(document).ready(function(){
//    $.ageCheck();
// });